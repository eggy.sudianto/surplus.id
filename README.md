Project berikut menggunakan Framework Laravel 10 dan database MySQL.

Cara penginstalan
1. clone project surplus.id
2. cd ke dalam project surplus.id
3. jalankan command "composer install"
4. jalankan command "cp .env.example .env"
5. buka file .env lalu set database di DB_DATABASE menjadi "surplus"
6. buka localhost/phpmyadmin di web browser
7. buat database dengan nama surplus
8. kembali ke command prompt dan jalankan command "php artisan migrate --seed" atau "php artisan migrate:refresh --seed". untuk melakukan migrasi table dan melakukan seed data
9. jalankan command "php artisan passport:install"
10. jalankan command "php artisan key:generate"
11. jalankan command "php artisan serve"

Penginstalan selesai. lanjut dengan mencoba API di postman
1. Buka postman
2. pilih tab collection lalu klik tombol import
3. upload file "Surplus.postman_collection.json"
4. tunggu hingga proses import selesai
5. pilih tab environments lalu klik tombol import
6. upload file "Surplus.postman_environment.json"
7. tunggu hingga proses import selesai
8. set active environemnt Surplus
9. pilih tab collection lalu pilih collection surplus dan pilih API "1. Register"
10. pilih tab body - raw, kemudian sesuaikan inputan datanya
11. pilih API "2. Login", kemudian sesuaikan credential loginnya sesuai data yang didaftarkan di tahap sebelumnya
12. copy string access_token yang ada pada field "token"
13. pilih tab Environments, pilih environments Surplus, dan set CURRENT VALUE pada variable token dengan token yang sudah di copy saat login
14. pilih tab collection
15. jalankan API lain sesuai yang diinginkan

NOTE : Wajib melakukan autentikasi login untuk dapat mengakses API.