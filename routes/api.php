<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\RegisterController;
use App\Http\Controllers\Api\LoginController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\CategoryProductController;
use App\Http\Controllers\Api\ImageController;
use App\Http\Controllers\Api\ProductImageController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('/register', [RegisterController::class, 'register']);
Route::post('/login', [LoginController::class, 'login'])->name("login");

Route::get('/401', function () {
    return response()->json([
        'error' => 'Unauthorized',
        'message' => 'Anda belum melakukan autentikasi.',
    ], 401);
})->name("401");


Route::middleware('auth:api')->group( function () {
    Route::post('/logout', [LoginController::class, 'logout']);

    Route::group(['prefix' => '/category', 'controller' => CategoryController::class], function () {
        Route::get('/', 'index')->name("category");
        Route::post('/', 'store')->name("category.store");
        Route::put('/{id}', 'update')->name("category.update");
        Route::delete('/{id}', 'delete')->name("category.delete");
        Route::get('/detail/{id}', 'detail')->name("category.detail");
    });

    Route::group(['prefix' => '/product', 'controller' => ProductController::class], function () {
        Route::get('/', 'index')->name("product");
        Route::post('/', 'store')->name("product.store");
        Route::put('/{id}', 'update')->name("product.update");
        Route::delete('/{id}', 'delete')->name("product.delete");
        Route::get('/detail/{id}', 'detail')->name("product.detail");
    });

    Route::group(['prefix' => '/categoryproduct', 'controller' => CategoryProductController::class], function () {
        Route::get('/', 'index')->name("categoryproduct");
        Route::post('/', 'store')->name("categoryproduct.store");
        Route::put('/', 'update')->name("categoryproduct.update");
        Route::delete('/', 'delete')->name("categoryproduct.delete");
        Route::post('/detail', 'detail')->name("categoryproduct.detail");
    });

    Route::group(['prefix' => '/image', 'controller' => ImageController::class], function () {
        Route::get('/', 'index')->name("image");
        Route::post('/', 'store')->name("image.store");
        Route::post('/{id}', 'update')->name("image.update");
        Route::delete('/{id}', 'delete')->name("image.delete");
        Route::get('/detail/{id}', 'detail')->name("image.detail");
    });

    Route::group(['prefix' => '/productimage', 'controller' => ProductImageController::class], function () {
        Route::get('/', 'index')->name("productimage");
        Route::post('/', 'store')->name("productimage.store");
        Route::put('/', 'update')->name("productimage.update");
        Route::delete('/', 'delete')->name("productimage.delete");
        Route::post('/detail', 'detail')->name("productimage.detail");
    });

});