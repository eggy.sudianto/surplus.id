<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController
{    
    /**
     * register
     *
     * @param  mixed $request
     * @return void
     */
    public function register(Request $request)
    {
        try{
            $json = file_get_contents('php://input');
            $datas = json_decode($json, true);

            $name = $datas["name"];
            $email = $datas["email"];
            $password = $datas["password"];

            $validator = Validator::make($datas, [
                'name'      => 'required',
                'email'     => 'required|email|unique:users',
                'password'  => 'required|min:8|confirmed'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            }

            $user = User::create([
                'name'      => $name,
                'email'     => $email,
                'password'  => Hash::make($password)
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Register Success!',
                'data'    => $user  
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }
}