<?php

namespace App\Http\Controllers\Api;

use App\Models\ProductImage;
use App\Models\Product;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;

class ProductImageController
{
    public function index()
    {
       try{
            $data = ProductImage::select(DB::raw("products.id product_id, products.name product_name, products.description product_description, products.enable product_enable, images.id images_id, images.name image_name, CONCAT('" . url('/image/') . "/', images.file) AS image_file, images.enable image_enable"))
                                ->join("images", "images.id","product_images.image_id")
                                ->join("products", "products.id","product_images.product_id")
                                ->get();

            return response()->json([
                'success' => true,
                'result' => $data
            ],200);
       } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ],500);
       }
    }

    public function detail(Request $request)
    {
        try{
            $json = file_get_contents('php://input');
            $datas = json_decode($json, true);

            $searchBy = $datas['searchBy'];
            $keywords = $datas['keywords'];

            $productImage = ProductImage::select(DB::raw("products.id product_id, products.name product_name, products.description product_description, products.enable product_enable, images.id images_id, images.name image_name, CONCAT('" . url('/image/') . "/', images.file) AS image_file, images.enable image_enable"));
            $productImage->join("images", "images.id","product_images.image_id");
            $productImage->join("products", "products.id","product_images.product_id");
            if($searchBy=="product"){
                $productImage->where("products.name","like","%$keywords%");
            }else if($searchBy=="image"){
                $productImage->where("images.name","like","%$keywords%");
            }else{
                 return response()->json([
                    'success' => false,
                    'result' => "Search By hanya berupa 'product' atau 'image'"
                ],500);
            }
            $data = $productImage->get();

            if (!$productImage) {
                return response()->json([
                    'success' => false,
                    'result' => "Data tidak ditemukan"
                ],404);
            }

            return response()->json([
                'success' => true,
                'result' => $data
            ],200);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ],500);
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try{
            $json = file_get_contents('php://input');
            $datas = json_decode($json, true);

            $product_id = $datas['product_id'];
            $image_id = $datas['image_id'];

            $validator = Validator::make($datas, [
                'image_id' => [
                    'required',
                    function ($attribute, $value, $fail) {
                        $image = Image::find($value);

                        if (!$image) {
                            $fail('Image tidak ditemukan');
                        } elseif (!$image->enable) {
                            $fail('Data Image tidak aktif, silakan pilih image lain');
                        }
                    },
                ],
                'product_id' => [
                    'required',
                    function ($attribute, $value, $fail) {
                        $product = Product::find($value);

                        if (!$product) {
                            $fail('Product tidak ditemukan');
                        } elseif (!$product->enable) {
                            $fail('Data Product tidak aktif, silakan pilih product lain');
                        }
                    },
                ],
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            }

            $insert = ProductImage::create([
                'image_id' => $image_id,
                'product_id' => $product_id,
            ]);

            return response()->json([
                'success' => true,
                'result' => $insert
            ],200);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ],500);
        }
    }


    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        try{
            $json = file_get_contents('php://input');
            $datas = json_decode($json, true);

            $image_id_old = $datas['image_id_old'];
            $product_id_old = $datas['product_id_old'];
            $image_id = $datas['image_id'];
            $product_id = $datas['product_id'];

            $validator = Validator::make($datas, [
                'image_id' => [
                    'required',
                    function ($attribute, $value, $fail) {
                        $image = Image::find($value);

                        if (!$image) {
                            $fail('Image tidak ditemukan');
                        } elseif (!$image->enable) {
                            $fail('Data Image tidak aktif, silakan pilih image lain');
                        }
                    },
                ],
                'product_id' => [
                    'required',
                    function ($attribute, $value, $fail) {
                        $product = Product::find($value);

                        if (!$product) {
                            $fail('Product tidak ditemukan');
                        } elseif (!$product->enable) {
                            $fail('Data Product tidak aktif, silakan pilih product lain');
                        }
                    },
                ],
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            }

            $productImage = ProductImage::where("image_id",$image_id_old)
                                            ->where("product_id",$product_id_old)
                                            ->get();
            if (count($productImage)==0) {
                return response()->json([
                    'success' => false,
                    'result' => "Data tidak ditemukan"
                ],404);
            }

            ProductImage::where("image_id",$image_id_old)
                            ->where("product_id",$product_id_old)
                            ->update([
                                'image_id' => $image_id,
                                'product_id' => $product_id,
                            ]);

            $updatedproductImage = ProductImage::select(DB::raw("products.id product_id, products.name product_name, products.description product_description, products.enable product_enable, images.id images_id, images.name image_name, CONCAT('" . url('/image/') . "/', images.file) AS image_file, images.enable image_enable"))
                                ->join("images", "images.id","product_images.image_id")
                                ->join("products", "products.id","product_images.product_id")
                                ->where("image_id",$image_id)
                                ->where("product_id",$product_id)
                                ->get();

            return response()->json([
                'success' => true,
                'result' => $updatedproductImage
            ],200);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ],500);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function delete()
    {
        try{
            $json = file_get_contents('php://input');
            $datas = json_decode($json, true);

            $image_id = $datas['image_id'];
            $product_id = $datas['product_id'];

            ProductImage::where("image_id",$image_id)
                            ->where("product_id",$product_id)
                            ->delete();

            return response()->json([
                'success' => true,
            ],200);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ],500);
        }
    }
}
