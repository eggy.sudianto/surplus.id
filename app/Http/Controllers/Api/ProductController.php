<?php

namespace App\Http\Controllers\Api;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController
{
    public function index()
    {
       try{
            $json = file_get_contents('php://input');
            $datas = json_decode($json, true);

            $enable = $datas['enable'] ?? true ;
            $data = Product::where("enable",$enable)->get();

            return response()->json([
                'success' => true,
                'result' => $data
            ],200);
       } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ],500);
       }
    }

    public function detail(Request $request, $id)
    {
        try{
            $product = Product::find($id);
            if (!$product) {
                return response()->json([
                    'success' => false,
                    'result' => "Data tidak ditemukan"
                ],404);
            }

            return response()->json([
                'success' => true,
                'result' => $product
            ],200);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ],500);
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try{
            $json = file_get_contents('php://input');
            $datas = json_decode($json, true);

            $name = $datas['name'];
            $description = $datas['description'];
            $enable = $datas['enable'];

            $validator = Validator::make($datas, [
                'name' => 'required',
                'description' => 'required',
                'enable' => 'required|boolean'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            }

            $insert = Product::create([
                'name' => $name,
                'description' => $description,
                'enable' => $enable,
            ]);

            return response()->json([
                'success' => true,
                'result' => $insert
            ],200);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ],500);
        }
    }


    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        try{
            $json = file_get_contents('php://input');
            $datas = json_decode($json, true);

            $name = $datas['name'];
            $description = $datas['description'];
            $enable = $datas['enable'];

            $validator = Validator::make($datas, [
                'name' => 'required',
                'description' => 'required',
                'enable' => 'required|boolean'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            }

            $product = Product::find($id);
            if (!$product) {
                return response()->json([
                    'success' => false,
                    'result' => "Data tidak ditemukan"
                ],404);
            }

            Product::find($id)->update([
                'name' => $name,
                'description' => $description,
                'enable' => $enable,
            ]);

            $updatedProduct = Product::find($id);

            return response()->json([
                'success' => true,
                'result' => $updatedProduct
            ],200);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ],500);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function delete($id)
    {
        try{
            Product::find($id)->delete();

            return response()->json([
                'success' => true,
            ],200);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ],500);
        }
    }
}
