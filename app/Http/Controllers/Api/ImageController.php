<?php

namespace App\Http\Controllers\Api;

use App\Models\Image;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File; 

class ImageController
{
    public function index()
    {
       try{
            $json = file_get_contents('php://input');
            $datas = json_decode($json, true);

            $enable = $datas['enable'] ?? true ;
            $data = Image::select(DB::raw("id, name, CONCAT('" . url('/image/') . "/', file) AS file"))
                            ->where("enable",$enable)->get();

            return response()->json([
                'success' => true,
                'result' => $data
            ],200);
       } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ],500);
       }
    }

    public function detail(Request $request, $id)
    {
        try{
            $image = Image::select(DB::raw("id, name, CONCAT('" . url('/image/') . "/', file) AS file"))
                            ->where("id",$id)->first();
            if (!$image) {
                return response()->json([
                    'success' => false,
                    'result' => "Data tidak ditemukan"
                ],404);
            }

            return response()->json([
                'success' => true,
                'result' => $image
            ],200);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ],500);
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try{
            $name = $_POST["name"];        
            $enable = $_POST["enable"];
           
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'enable' => 'required|boolean',
                'file' => 'required|file',
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            }
            
            if(isset($_FILES['file']['name'])){
                $filename =  date("YmdHis") . rand(1,9999) . "." . $request->file('file')->getClientOriginalExtension();

                move_uploaded_file($request->file('file'), public_path("/image/").$filename);
            }

            $insert = Image::create([
                'name' => $name,
                'file' => $filename,
                'enable' => $enable,
            ]);

            $data = Image::select(DB::raw("id, name, CONCAT('" . url('/image/') . "/', file) AS file"))
                            ->where("id",$insert->id)->get();

            return response()->json([
                'success' => true,
                'result' => $data
            ],200);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ],500);
        }
    }


    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        try{
            $name = $request->input('name');        
            $enable = $request->enable;
           
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'enable' => 'required|boolean',
                'file' => 'required|file',
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            }
            
            $image = Image::where("id",$id)->first();
            if (!$image) {
                return response()->json([
                    'success' => false,
                    'result' => "Data tidak ditemukan"
                ],404);
            }

            if(isset($_FILES['file']['name'])){
                if (file_exists(public_path("image/").$image->file)) {
                    File::delete(public_path("image/").$image->file);
                }

                $filename =  date("YmdHis") . rand(1,9999) . "." . $request->file('file')->getClientOriginalExtension();

                move_uploaded_file($request->file('file'), public_path("/image/").$filename);
            }

            Image::find($id)->update([
                'name' => $name,
                'file' => $filename,
                'enable' => $enable,
            ]);

            $data = Image::select(DB::raw("id, name, CONCAT('" . url('/image/') . "/', file) AS file"))
                            ->where("id",$id)->get();

            return response()->json([
                'success' => true,
                'result' => $data
            ],200);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ],500);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function delete($id)
    {
        try{
            $image = Image::where("id",$id)->first();

            if (file_exists(public_path("image/").$image->file)) {
                File::delete(public_path("image/").$image->file);
            }

            Image::find($id)->delete();

            return response()->json([
                'success' => true,
            ],200);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ],500);
        }
    }
}
