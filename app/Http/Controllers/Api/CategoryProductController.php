<?php

namespace App\Http\Controllers\Api;

use App\Models\CategoryProduct;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryProductController
{
    public function index()
    {
       try{
            $data = CategoryProduct::select("categorys.name as category_name", "products.name as product_name")
                                ->join("categorys", "categorys.id","category_products.category_id")
                                ->join("products", "products.id","category_products.product_id")
                                ->get();

            return response()->json([
                'success' => true,
                'result' => $data
            ],200);
       } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ],500);
       }
    }

    public function detail(Request $request)
    {
        try{
            $json = file_get_contents('php://input');
            $datas = json_decode($json, true);

            $searchBy = $datas['searchBy'];
            $keywords = $datas['keywords'];

            $categoryProduct = CategoryProduct::select("categorys.name as category_name", "products.name as product_name");
            $categoryProduct->join("categorys", "categorys.id","category_products.category_id");
            $categoryProduct->join("products", "products.id","category_products.product_id");
            if($searchBy=="category"){
                $categoryProduct->where("categorys.name","like","%$keywords%");
            }else if($searchBy=="product"){
                $categoryProduct->where("products.name","like","%$keywords%");
            }else{
                 return response()->json([
                    'success' => false,
                    'result' => "Search By hanya berupa 'category' atau 'product'"
                ],500);
            }
            $data = $categoryProduct->get();

            if (!$categoryProduct) {
                return response()->json([
                    'success' => false,
                    'result' => "Data tidak ditemukan"
                ],404);
            }

            return response()->json([
                'success' => true,
                'result' => $data
            ],200);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ],500);
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try{
            $json = file_get_contents('php://input');
            $datas = json_decode($json, true);

            $category_id = $datas['category_id'];
            $product_id = $datas['product_id'];

            $validator = Validator::make($datas, [
                'category_id' => [
                    'required',
                    function ($attribute, $value, $fail) {
                        $category = Category::find($value);

                        if (!$category) {
                            $fail('Category tidak ditemukan');
                        } elseif (!$category->enable) {
                            $fail('Data Category tidak aktif, silakan pilih category lain');
                        }
                    },
                ],
                'product_id' => [
                    'required',
                    function ($attribute, $value, $fail) {
                        $product = Product::find($value);

                        if (!$product) {
                            $fail('Product tidak ditemukan');
                        } elseif (!$product->enable) {
                            $fail('Data Product tidak aktif, silakan pilih product lain');
                        }
                    },
                ],
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            }

            $insert = CategoryProduct::create([
                'category_id' => $category_id,
                'product_id' => $product_id,
            ]);

            return response()->json([
                'success' => true,
                'result' => $insert
            ],200);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ],500);
        }
    }


    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        try{
            $json = file_get_contents('php://input');
            $datas = json_decode($json, true);

            $category_id_old = $datas['category_id_old'];
            $product_id_old = $datas['product_id_old'];
            $category_id = $datas['category_id'];
            $product_id = $datas['product_id'];

            $validator = Validator::make($datas, [
                'category_id' => [
                    'required',
                    function ($attribute, $value, $fail) {
                        $category = Category::find($value);

                        if (!$category) {
                            $fail('Category tidak ditemukan');
                        } elseif (!$category->enable) {
                            $fail('Data Category tidak aktif, silakan pilih category lain');
                        }
                    },
                ],
                'product_id' => [
                    'required',
                    function ($attribute, $value, $fail) {
                        $product = Product::find($value);

                        if (!$product) {
                            $fail('Product tidak ditemukan');
                        } elseif (!$product->enable) {
                            $fail('Data Product tidak aktif, silakan pilih product lain');
                        }
                    },
                ],
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            }

            $categoryProduct = CategoryProduct::where("category_id",$category_id_old)
                                            ->where("product_id",$product_id_old)
                                            ->get();
            if (count($categoryProduct)==0) {
                return response()->json([
                    'success' => false,
                    'result' => "Data tidak ditemukan"
                ],404);
            }

            CategoryProduct::where("category_id",$category_id_old)
                            ->where("product_id",$product_id_old)
                            ->update([
                                'category_id' => $category_id,
                                'product_id' => $product_id,
                            ]);

            $updatedCategoryProduct = CategoryProduct::select("categorys.name as category_name", "products.name as product_name")
                                ->join("categorys", "categorys.id","category_products.category_id")
                                ->join("products", "products.id","category_products.product_id")
                                ->where("category_id",$category_id)
                                ->where("product_id",$product_id)
                                ->get();

            return response()->json([
                'success' => true,
                'result' => $updatedCategoryProduct
            ],200);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ],500);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function delete()
    {
        try{
            $json = file_get_contents('php://input');
            $datas = json_decode($json, true);

            $category_id = $datas['category_id'];
            $product_id = $datas['product_id'];

            CategoryProduct::where("category_id",$category_id)
                            ->where("product_id",$product_id)
                            ->delete();

            return response()->json([
                'success' => true,
            ],200);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ],500);
        }
    }
}
