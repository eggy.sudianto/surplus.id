<?php

namespace App\Http\Controllers\Api;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
       try{
            $json = file_get_contents('php://input');
            $datas = json_decode($json, true);

            $enable = $datas['enable'] ?? true ;
            $data = Category::where("enable",$enable)->get();

            return response()->json([
                'success' => true,
                'result' => $data
            ],200);
       } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ],500);
       }
    }

    public function detail(Request $request, $id)
    {
        try{

            $category = Category::find($id);
            if (!$category) {
                return response()->json([
                    'success' => false,
                    'result' => "Data tidak ditemukan"
                ],404);
            }

            return response()->json([
                'success' => true,
                'result' => $category
            ],200);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ],500);
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try{
            $json = file_get_contents('php://input');
            $datas = json_decode($json, true);

            $name = $datas['name'];
            $enable = $datas['enable'];

            $validator = Validator::make($datas, [
                'name' => 'required',
                'enable' => 'required|boolean'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            }

            $insert = Category::create([
                'name' => $name,
                'enable' => $enable,
            ]);

            return response()->json([
                'success' => true,
                'result' => $insert
            ],200);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ],500);
        }
    }


    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        try{
            $json = file_get_contents('php://input');
            $datas = json_decode($json, true);

            $name = $datas['name'];
            $enable = $datas['enable'];

            $validator = Validator::make($datas, [
                'name' => 'required',
                'enable' => 'required|boolean'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            }

            $category = Category::find($id);
            if (!$category) {
                return response()->json([
                    'success' => false,
                    'result' => "Data tidak ditemukan"
                ],404);
            }

            Category::find($id)->update([
                'name' => $name,
                'enable' => $enable,
            ]);

            $updatedCategory = Category::find($id);

            return response()->json([
                'success' => true,
                'result' => $updatedCategory
            ],200);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ],500);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function delete($id)
    {
        try{
            Category::find($id)->delete();

            return response()->json([
                'success' => true,
            ],200);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ],500);
        }
    }
}
