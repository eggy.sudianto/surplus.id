<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Category;

class Category extends Model
{
    use HasFactory;

    protected $table = 'Categorys';

    protected $fillable = [
        "name",
        "enable",
    ];

    public function categorys()
    {
        return $this->belongsToMany(Category::class, 'category_products');
    }

}
